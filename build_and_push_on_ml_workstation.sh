rsync -av --delete -e ssh . woelfle@iik-gamora.fzi.de:/tmp/docker_envs-pytorch-notebook-build-only/ --exclude=.git
ssh woelfle@iik-gamora.fzi.de "cd /tmp/docker_envs-pytorch-notebook-build-only/ && bash build_docker_image.sh"
ssh woelfle@iik-gamora.fzi.de "cd /tmp/docker_envs-pytorch-notebook-build-only/ && docker push dwoelfle/pytorch-notebook:$(grep 'TAG=' build_docker_image.sh | cut -d = -f 2)"
ssh woelfle@iik-gamora.fzi.de "rm -rf cd /tmp/docker_envs-pytorch-notebook-build-only/"
