# Pytorch Notebook

This is a jupyter notebook with pytorch and a lot of other useful ML libs installed.



## Building Issues

This image is too large (> 10Gb) to be build on a shared Gitlab.com runner. 

To improve this issue you may want to refactor the image to use the official [nvidia image](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/pytorch), which at least prevents having to upload a 10Gb layer with PyTorch and Cuda every time something smallish has changed. However, the Nvidia image has some complicated license conditions.

It should also be noted that even if it would be possible to build this image on a shared runner, it would take rather long every time, as the normal docker-in-docker runners cannot cache. 

The current solution is hacky script that manually builds the image on groot.
