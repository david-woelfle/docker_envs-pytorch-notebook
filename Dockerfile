FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime

# Ports for jupyter
EXPOSE 8888

ENV DEBIAN_FRONTEND="noninteractive"

# Do the update first, it takes ages.
RUN apt-get update

# Create directories for notebooks. Seems that jupyter needs to write
# something to .local on startup.
RUN mkdir -p /notebooks /.local && \
    chmod a+rwx -R /notebooks /.local
WORKDIR /jupyter

# Install Jupyterlab.
RUN conda install jupyterlab -c conda-forge

# Install packages for Marie's forecasts
RUN conda install -c conda-forge statsforecast neuralforecast plotly ipywidgets &&  \
    pip install datasetsforecast

RUN jupyter --help
ENTRYPOINT [ "jupyter", "lab",  "--ip=0.0.0.0", "--port=8888", "--no-browser", "--notebook-dir=/notebooks/"]
CMD [ "--autoreload" ]


#ENTRYPOINT [ "jupyter", "lab" ]
#CMD [ "--help-all" ]

# # Install dependencies for OpenAI's gym, as listed here:
# # https://github.com/openai/gym/blob/c755d5c35a25ab118746e2ba885894ff66fb8c43/py.Dockerfile
# RUN apt-get install -y unzip libglu1-mesa-dev libgl1-mesa-dev libosmesa6-dev xvfb patchelf

# # Update ffmpeg from conda, it seems that pytorch brings it's own version but not all
# # dependencies, which is bad for gym.
# RUN conda update -c conda-forge ffmpeg -y

# # Install gym (as suggested in https://github.com/openai/gym) plus python
# # dependencies of gym (pyglet) and a package required for rendering environments
# # in headless mode (pyvirtualdisplay) and it's dependency.
# RUN cd ~ && \
#     pip3 install gym pyglet pyvirtualdisplay pillow


# # Install usefull python libs for machine learning and jupyter tools.
# # tensorboardX is used by ray.tune during hyperparameter tuneing.
# # boto3 is required for pushing stuff to mlflow artificat store.
# #
# # Note that pytorch-lightning and mlflow must match for auto-logging. See:
# # https://www.mlflow.org/docs/latest/python_api/mlflow.pytorch.html#module-mlflow.pytorch
# RUN conda install -y -c conda-forge pymc>=4
# RUN cd ~ && \
#     pip3 install pytest pydantic nbzip cma gpytorch ray[tune] tensorboardx pytorch-lightning mlflow boto3


# # Install test notebooks
# RUN mkdir "${HOME}"/test_notebooks
# COPY ./test_notebooks/ "${HOME}"/test_notebooks
